import { assert } from "chai";

const doctors = [
    {
        name: "House"
    }, 
    {
        name: "Octopus"
    }
];

export function getAvailableDoctors() {
    return doctors;
}

assert.equal(getAvailableDoctors(), doctors);
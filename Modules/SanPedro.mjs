import { assert } from "chai";

const doctors = [
    {
        name: "Strange"
    },
    {
        name: "Who"
    }
];

export function getAvailableDoctors() {
    return doctors;
}

assert.equal(getAvailableDoctors(), doctors);
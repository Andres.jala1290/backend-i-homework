import { expect } from "chai";
import { retrieveDoctors } from "../University.mjs"

const morningDocs = [
    {
        name: "Strange"
    },
    {
        name: "Who"
    }
];

const nightDocs = [
    {
        name: "House"
    }, 
    {
        name: "Octopus"
    }
];

const docs = await retrieveDoctors();

describe('University Test', function() {

    it('check doctors', function() {
        const today = new Date();
        const hour = today.getHours();
 
        if(hour > 6 && hour < 19) {
            expect(docs).to.deep.equal(morningDocs);
        } else {
            expect(docs).to.deep.equal(nightDocs);            
        }
    })
})
export async function retrieveDoctors() {
    const today = new Date();

    const hour = today.getHours();

    let hospital;
    
    if(hour > 6 && hour < 19) {
        hospital = await import("./SanPedro.mjs");
    } else {
        hospital = await import("./LosOlivos.mjs");
    }

    return hospital.getAvailableDoctors();
}

console.log(await retrieveDoctors());
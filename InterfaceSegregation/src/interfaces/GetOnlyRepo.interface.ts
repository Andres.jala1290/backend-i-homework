export default interface GetOnlyRepo<T> {

    get(): T;
    
}
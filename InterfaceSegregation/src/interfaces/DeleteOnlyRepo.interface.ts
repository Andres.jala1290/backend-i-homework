export default interface DeleteOnlyRepo {

    delete(id: string): void;

}
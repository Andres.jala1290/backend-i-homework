export default interface UpdateOnlyRepo<T> {

    update(entity: T);

}
export default interface InsertOnlyRepo<T> {

    insert(entity: T);
    
}
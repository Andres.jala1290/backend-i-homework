import EventLog from "./eventLog";
import GetOnlyRepo from "./interfaces/GetOnlyRepo.interface";
import User from './user';

export default class LogRepository implements GetOnlyRepo<EventLog> {
    get(): EventLog {
        return new EventLog('1', 'test message');
    }
}
import GetOnlyRepo from "./interfaces/GetOnlyRepo.interface";
import User from './user';

export default class UserRepository implements GetOnlyRepo<User> {
    get(): User {
        return new User('1');
    }
}
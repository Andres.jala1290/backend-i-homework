import { Entity, Column, PrimaryColumn } from 'typeorm';
import { v4 as uuid} from 'uuid';

@Entity()
export class User {
    @PrimaryColumn()
    id: string = uuid();

    @Column()
    name: string;

    @Column()
    nickname: string;

    @Column()
    totalAttendance: number = 0;
}
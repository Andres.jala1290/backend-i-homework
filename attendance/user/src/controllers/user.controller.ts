import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { User } from "../entities/user.entity";
import axios from "axios";
import { sendUserDelete } from "../services/queueSender";

export const createUser = async(request: Request, response: Response): Promise<Response> => {
    const newUser = await getRepository(User).create(request.body);
    const result = await getRepository(User).save(newUser);
    return response.json(result);
}

export const getUsers = async(request: Request, response: Response): Promise<Response> => {
    const parameterCount = Object.keys(request.query).length;
    if(parameterCount === 0) {
        const result = await getRepository(User).find();
        return response.json(result);
    } else {
        const requestedNickname = request.query.nickname;
        const requestedName = request.query.name;
        if(requestedNickname === undefined && requestedName === undefined) {
            return response.sendStatus(400);
        }

        const nickname = requestedNickname !== undefined? '%' + requestedNickname + '%' : '%%';
        const name = requestedName !== undefined? '%' + requestedName + '%' : '%%';

        const result = await getRepository(User).createQueryBuilder('user').where(
            "nickname like :nickname and name like :name", 
            {
                nickname: nickname,
                name: name
            }
        ).getMany();

        return response.json(result);
    }
}

export const updateUserAttendance = async(request: Request, response: Response): Promise<Response> => {
    const userId = request.params.id;

    const userToUpdate = await getRepository(User).findOne(userId.toString());
    if(userToUpdate === undefined) {
        return response.sendStatus(400);
    }

    const totalAttendance = request.body.totalAttendance;

    if(totalAttendance === undefined) {
        return response.sendStatus(400);
    }

    userToUpdate.totalAttendance = Number.parseInt(totalAttendance.toString());
    const result = await getRepository(User).save(userToUpdate);

    return response.json(result);
}

export const deleteUser = async(request: Request, response: Response): Promise<Response> => {
    const userId = request.params.id;

    const user = await getRepository(User).findOne(userId);
    if(user === undefined) {
        return response.sendStatus(404);
    }
    sendUserDelete(userId);

    const result = await getRepository(User).delete(userId);
    return response.json(result);
}

export const getUserByID = async(request: Request, response: Response): Promise<Response> => {
    const userId = request.params.id;
    const url = `http://localhost:3001/attendance/${userId}`;

    let attendances;
    await axios.get(url)
        .then(response => {
            attendances = response.data;
        })
        .catch(error => {
            console.log(error);
        });

    const user = await getRepository(User).findOne(request.params.id);
    if(user === undefined) {
        return response.sendStatus(404);
    }

    const result = {
        //...user
        'id':user.id,
        'totalAttendance':user.totalAttendance,
        'name':user.name,
        'nickname':user.nickname,
        'attendance':attendances
    };
    
    return response.json(result);
}

export const test = async(request: Request, response: Response): Promise<Response> => {
    //sendAttendanceUpdate('TEST_ID');
    return response.sendStatus(200);
}
import { Router } from "express";
import { createUser, deleteUser, getUserByID, getUsers, updateUserAttendance, test } from "../controllers/user.controller";

const router = Router();

router.post('/users', createUser);
router.get('/users', getUsers);
router.delete('/users/:id', deleteUser);
router.get('/users/:id', getUserByID);
router.put('/users/:id', updateUserAttendance);
router.get('/test', test);

export default router;
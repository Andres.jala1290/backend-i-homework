import amqp = require('amqplib/callback_api');

export function sendUserDelete(userId: string) {
    amqp.connect(
        {
            protocol: 'amqp',
            hostname: 'localhost',
            port: 5672,
            username: 'attendance',
            password: '1290.Test$'
        },
        function(error0: any, connection: any) {
            if(error0) {
                throw error0;
            }

            connection.createChannel(function(error1: any, channel: any) {
                if(error1) {
                    throw error1;
                }

                const queue = 'user';
                var message = userId;
                const exchange = 'attendance-exchange';
                const severity = 'user';

                
                channel.assertQueue(queue, {
                    exclusive: false
                });
                channel.sendToQueue(queue, Buffer.from(message));
                
                channel.assertExchange(exchange, 'direct');
                channel.bindQueue(queue, exchange, severity);
            });

            setTimeout(function() {
                connection.close();
            }, 500);
        }
    )
}
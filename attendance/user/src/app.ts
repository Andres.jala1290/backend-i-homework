import express from 'express';
import morgan from 'morgan';
import cors from 'cors';
import userRoutes from './routes/user.routes';
import 'reflect-metadata';
import { createConnection } from 'typeorm';
import config from 'config';

const port: number = config.get('port');
const host: string = config.get('host');

const app = express();
createConnection();

app.use(cors());
app.use(morgan('dev'));
app.use(express.json());

app.use(userRoutes);

app.listen(port, host, () => {
    console.log(`Server listening at http://${host}:${port}`);
});
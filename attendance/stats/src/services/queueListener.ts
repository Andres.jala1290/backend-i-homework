import amqp = require('amqplib/callback_api');
import { fetchAttendancesById } from './attendanceHelper';
import { setUserAttendance } from './userHelper';

export function listenAttendanceQueue() {
    amqp.connect(
        {
            protocol: 'amqp',
            hostname: 'localhost',
            port: 5672,
            username: 'attendance',
            password: '1290.Test$'
        },
        function(error0: any, connection: any) {
            if(error0) {
                throw error0;
            }

            connection.createChannel(
                function(error1: any, channel: any) {
                    if(error1) {
                        throw error1;
                    }

                    const queue = 'attendance';
                    const exchange = 'attendance-exchange';
                    const severity = 'attendance';

                    channel.assertExchange(exchange, 'direct', {
                        durable: true
                    });

                    channel.assertQueue(queue, {
                        exclusive: false
                    }, function(error2: any, q: any) {
                        if(error2) {
                            throw error2;
                        }

                        console.log(' [*] Waiting for attendance.');

                        channel.bindQueue(q.queue, exchange, severity);

                        channel.consume(q.queue, async function(message: any) {
                            const userId = message.content.toString();
                            const attendanceCount = await fetchAttendancesById(userId);
                            setUserAttendance(userId, attendanceCount);
                        }, {
                            noAck: true
                        });
                    })
                }
            );
        }
    )
}

export function listenUserQueue() {
    amqp.connect(
        {
            protocol: 'amqp',
            hostname: 'localhost',
            port: 5672,
            username: 'attendance',
            password: '1290.Test$'
        },
        function(error0: any, connection: any) {
            if(error0) {
                throw error0;
            }

            connection.createChannel(
                function(error1: any, channel: any) {
                    if(error1) {
                        throw error1;
                    }

                    const queue = 'user';
                    const exchange = 'attendance-exchange';
                    const severity = 'user';

                    channel.assertExchange(exchange, 'direct', {
                        durable: true
                    });

                    channel.assertQueue(queue, {
                        exclusive: false
                    }, function(error2: any, q: any) {
                        if(error2) {
                            throw error2;
                        }

                        console.log(' [*] Waiting for user.');

                        channel.bindQueue(q.queue, exchange, severity);

                        channel.consume(q.queue, async function(message: any) {
                            const userId = message.content.toString();
                            console.log('user got a message with value:', userId);
                        }, {
                            noAck: true
                        });
                    })
                }
            );
        }
    )
}
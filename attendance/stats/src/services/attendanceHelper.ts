import axios from "axios";

export async function fetchAttendancesById(userId: string): Promise<number> {
    const url = `http://localhost:3001/attendance/${userId}`;

    let attendances = [];
    await axios.get(url)
        .then(response => {
            attendances = response.data;
        })
        .catch(error => {
            console.log(error);
        });

    const attendanceCount = attendances.length;

    return attendanceCount;
}
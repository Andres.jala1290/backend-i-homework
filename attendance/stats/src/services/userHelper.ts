import axios from 'axios';

export async function setUserAttendance(userId: string, attendanceCount: number) {
    const url = `http://localhost:3000/users/${userId}`;

    const result = await axios.put(url, {
        totalAttendance: attendanceCount
    });

    console.log(result);
}
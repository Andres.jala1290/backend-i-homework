import express from 'express';
import cors from 'cors';
import config from 'config';
import { listenAttendanceQueue, listenUserQueue } from './services/queueListener';

const port: number = config.get('port');
const host: string = config.get('host');

const app = express();

app.use(cors());
app.use(express.json());


app.listen(port, host, async () => {
    console.log(`Server listening at http://${host}:${port}`);
    
    listenAttendanceQueue();
    listenUserQueue();
});
import { Router } from "express";
import { getAttendanceByUserID, getAttendances, addAttendance, deleteAttendance, test } from "../controllers/attendance.controller";

const router = Router();

router.get('/attendance', getAttendances);
router.get('/attendance/:id', getAttendanceByUserID);
router.post('/attendance', addAttendance);
router.delete('/attendance/:id', deleteAttendance);
router.get('/test', test);

export default router;
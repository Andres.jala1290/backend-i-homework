import { Request, Response } from "express";
import AttendanceModel from '../models/attendance.model';
import { sendUserAttendanceUpdate } from "../services/queueSender";

export const getAttendances = async(request: Request, response: Response): Promise<Response> => {
    const result = await AttendanceModel.find();
    return response.json(result);
}

export const getAttendanceByUserID = async(request: Request, response: Response): Promise<Response> => {
    const result = await AttendanceModel.find({ userId:request.params.id });
    return response.json(result);
}

export const addAttendance = async(request: Request, response: Response): Promise<Response> => {
    const result = await AttendanceModel.create(request.body);
    sendUserAttendanceUpdate(request.body.userId);
    return response.json(result);
}

export const deleteAttendance = async(request: Request, response: Response): Promise<Response> => {
    const attendanceId = request.params.id;
    const userId = request.body.userId;

    if(attendanceId === undefined || userId === undefined) {
        return response.sendStatus(400);
    }

    const result = await AttendanceModel.remove({
        _id:attendanceId,
        userId:userId
    });
        
    sendUserAttendanceUpdate(request.body.userId);
    return response.json(result);
}

export const test = async(request: Request, response: Response): Promise<Response> => {
    return response.sendStatus(200);
}
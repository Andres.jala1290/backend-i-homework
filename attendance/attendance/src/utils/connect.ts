import mongoose from "mongoose";
import config from "config";

async function connect() {
    const dbUri: string = config.get('dbUri');

    try {
        await mongoose.connect(dbUri);
        console.log('Connected to DB');
    } catch(error) {
        console.error('Could not connect to DB');
        process.exit(1);
    }
}

export default connect;
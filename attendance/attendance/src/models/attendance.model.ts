import mongoose from 'mongoose';

const attendanceSchema = new mongoose.Schema({
    userId: { type: String, required: true },
    startTime: { type: String, required: true },
    endTime: { type: String },
    date: { type: Date, required: true },
    notes: { type: String }
});

const AttendanceModel = mongoose.model('Attendance', attendanceSchema);

export default AttendanceModel;
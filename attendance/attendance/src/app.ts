import express from 'express';
import cors from 'cors';
import config from 'config';
import connect from './utils/connect';
import attendanceRouter from './routes/attendance.routes';

const port: number = config.get('port');
const host: string = config.get('host');

const app = express();

app.use(cors());
app.use(express.json());

app.use(attendanceRouter);

app.listen(port, host, async () => {
    console.log(`Server listening at http://${host}:${port}`);

    await connect();
});
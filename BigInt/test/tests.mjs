import { expect } from "chai";
import { deserialize } from "../bigIntHelper.mjs";
import { serialize } from "../bigIntHelper.mjs";

const myObj = {
    testNumber: 123,
    testBigInt: 987n,
    testNested: {
        myBigIntProp: 5n,
        myNumberProp: 10,
        myArrayProp: [5n],
        myObjectProp: {
            theDeepestProp: 5n
        }
    },
   testArray: [5, 50n]
}

const myString = `{"testNumber":123,"testBigInt":"987n","testNested":{"myBigIntProp":"5n","myNumberProp":10,"myArrayProp":["5n"],"myObjectProp":{"theDeepestProp":"5n"}},"testArray":[5,"50n"]}`;

describe('serialize test' , function() {

    it('serialize complex object', function() {
        expect(myString).to.deep.equal(serialize(myObj));
    })

    /*
    // assert won't equal objects, since two created objects
    // even if they are the same, they point to different objects
    // thus, we have to use expect().to.deep.equal()
    */

    it('deserialize complex object', function() {
        expect(myObj).to.deep.equal(deserialize(myString));
    })

})


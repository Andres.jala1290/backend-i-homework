/*
*/
export function serialize2(customObject) {
    return "{" + innerSerialize(customObject) + "}";
}

function innerSerialize(customObject) {
    let jsonString = "";
    
    for (const property in customObject) {
        jsonString += `"${property}":${innerSingleSerialize(customObject[property])},`;
    }
    jsonString = jsonString.slice(0, -1);
    
    return jsonString;
}

function innerSingleSerialize(element) {
    if (typeof element === 'object') {
        if(Array.isArray(element)) {
            return `[${innerSerialize(element)}]`;
        } else {
            return `{${innerSerialize(element)}}`;
        }
    } else {
        if (typeof element === 'number') {
            return element.toString();
        } else if (typeof element === 'bigint') {
            return `"${element.toString()}n"`;
        } else {
            return `"${element.toString()}"`;
        }
    }
}

function replacer (key, value) {
    if(typeof value === 'bigint') {
        return value.toString() + "n";
    }
    return value;
}

export function serialize(customObject) {
    return JSON.stringify(customObject, replacer);
}

function reviewer(key, value) {
    if (typeof value === "string" && /^\d+n$/.test(value)) {
        return BigInt(value.substr(0, value.length - 1))
    }
    return value;
}

export function deserialize (jsonString) {
    const jsonObject = JSON.parse(jsonString, reviewer);

    return jsonObject;
}
// Complex Objects
const myObj = {
    testNumber: 123,
    testBigInt: 987n,
    testNested: {
        myBigIntProp: 5n,
        myNumberProp: 10,
        myArrayProp: [5n],
        myObjectProp: {
            theDeepestProp: 5n
        }
    },
   testArray: [5, 50n]
}

import { deserialize } from "./bigIntHelper.mjs";
import { serialize, serialize2 } from "./bigIntHelper.mjs";

//const jsonString2 = serialize2(myObj);
//console.log("first: " + jsonString2);

const jsonString = serialize(myObj);

console.log(jsonString);

const jsonObject = deserialize(jsonString);

console.log(jsonObject);
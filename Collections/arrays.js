// Array Creation
const exampleArray = ["first", "second", "third"];
console.log(exampleArray);

// Iterating an Array
// We can iterate over an array using a simple for
for(index = 0; index < exampleArray.length; index++) {
    exampleArray[index];
}

// We can also iterate over an array using the .forEach method
exampleArray.forEach(positionValue => {
    console.log(positionValue);
})

// Multidimentional Arrays
const exampleMultiDimention = [["1.1", "1.2"], ["2.1", "2.2"], ["3.1", "3.2"]];
console.log(exampleMultiDimention);

// Adding and removing elements
// Adding an element to the end of the Array
exampleArray.push('fourth');
console.log(exampleArray);

// Removing an element from the end of the array
exampleArray.pop();
console.log(exampleArray);

// Adding an element to the beginning of the array
exampleArray.unshift("zero");
console.log(exampleArray);

// Remove an item from the beginning of the array
exampleArray.shift();
console.log(exampleArray);

// Add an item from a position of an array
exampleArray.splice(0, 0, "zero");
console.log(exampleArray);

// Replace an item from a position of an array
exampleArray.splice(3, 1, "not third");
console.log(exampleArray);

// Remove an item from a position of an array
exampleArray.splice(3, 1);
console.log(exampleArray);

// .find()
const found = exampleArray.find(element => element === "zero");
console.log(found);

// .map()
const map = exampleArray.map(element => element += " mapped");
console.log(map);

// .filter()
const filter = exampleArray.filter(word => word.includes("e"));
console.log(filter);

// .flatMap()
// The flatMap() method returns a new array formed by applying a given callback function to each element
// of the array, and then flattening the result by one level. It is identical to a map() followed by a flat() of
// depth 1, but slightly more efficient than calling those two methods separately.
const flatmap = exampleArray.flatMap(word => word + " flat mapped");
console.log(flatmap);

// .sort()
exampleArray.sort();
console.log(exampleArray);

// others
//slice
console.log(exampleArray.slice(2));

// .includes()
console.log(exampleArray.includes("zero"));

// .reduce()
const numbersArray = [1, 2, 3, 4];
const reducer = (previousValue, currentValue) => previousValue + currentValue;
console.log(numbersArray.reduce(reducer));

// .reverse()
const reversed = exampleArray.reverse();
console.log('reversed:', reversed);

/*
A TypedArray object describes an array-like view of an underlying binary data buffer. 
There is no global property named TypedArray, nor is there a directly visible TypedArray constructor. 
Instead, there are a number of different global properties, whose values are typed array constructors 
for specific element types, listed below. 
On the following pages you will find common properties and methods that can be used with any typed 
array containing elements of any type.
*/

// create a TypedArray with a size in bytes
const typedArray1 = new Int8Array(8);
typedArray1[0] = 32;

const typedArray2 = new Int8Array(typedArray1);
typedArray2[1] = 42;

console.log(typedArray1);
// expected output: Int8Array [32, 0, 0, 0, 0, 0, 0, 0]

console.log(typedArray2);
// expected output: Int8Array [32, 42, 0, 0, 0, 0, 0, 0]
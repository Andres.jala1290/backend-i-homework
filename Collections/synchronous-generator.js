//Synchronous generators are special versions of function definitions and method definitions that always return synchronous iterables:

// Generator function declaration
function* genFunc1() { /*···*/ }

// Generator function expression
const genFunc2 = function* () { /*···*/ };

// Generator method definition in an object literal
const obj = {
  * generatorMethod() {
    // ···
  }
};

// Generator method definition in a class definition
// (class declaration or class expression)
class MyClass {
  * generatorMethod() {
    // ···
  }
}

/*
Asterisks (*) mark functions and methods as generators:

- Functions: The pseudo-keyword function* is a combination of the keyword function and an asterisk.
- Methods: The * is a modifier (similar to static and get).
*/
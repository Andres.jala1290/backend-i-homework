import Book from "./book";

export default class Invoice {
  private total: number;

  constructor(
    private book: Book,
    private quantity: number,
    private discountRate: number,
    private taxRate: number
  ) {}

  private calculateTotal() {
    this.total = 100;
  }

  getTotal() {
    this.calculateTotal();
    return this.total;
  }

  getBook() {
    return this.book;
  }

  getQuantity() {
    return this.quantity;
  }

  getDiscountRate() {
    return this.discountRate;
  }

  getTaxRate() {
    return this.taxRate;
  }
}
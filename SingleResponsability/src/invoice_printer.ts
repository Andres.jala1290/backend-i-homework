import Invoice from "./invoice";

export default class InvoicePrint {
    printInvoice(invoice: Invoice) {
      console.log(`${invoice.getBook().getName()}`);
      console.log(`Invoice quantity: ${invoice.getQuantity()}`);
      console.log(`Discount Rate: ${invoice.getDiscountRate()}`);
      console.log(`Tax Rate: ${invoice.getTaxRate()}`);
      console.log(`TOTAL: ${invoice.getTotal()}`);
  }
}
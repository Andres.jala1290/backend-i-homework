import { Express, Request, Response } from 'express';
import { createPlayerHandler } from './controllers/player.controller';
import {
  createPlayerSessionHandler,
  deleteSessionHandler,
  getPlayerSessionsHandler
} from './controllers/session.controller';
import requirePlayer from './middleware/requirePlayer';
import validateResource from './middleware/validateResource';
import { createPlayerSchema } from './schema/player.schema';
import { createSessionSchema } from './schema/session.schema';

function routes(app: Express) {
  app.get('/healthcheck', (request: Request, response: Response) =>
    response.sendStatus(200)
  );

  app.post(
    '/api/players',
    validateResource(createPlayerSchema),
    createPlayerHandler
  );

  app.post(
    '/api/sessions',
    validateResource(createSessionSchema),
    createPlayerSessionHandler
  );

  app.get('/api/sessions', requirePlayer, getPlayerSessionsHandler);

  app.delete('/api/sessions', requirePlayer, deleteSessionHandler);
}

export default routes;

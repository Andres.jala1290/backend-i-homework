import { Request, Response } from 'express';
import { CreatePlayerInput } from '../schema/player.schema';
import { createPlayer } from '../services/player.service';

export async function createPlayerHandler(
  request: Request<{}, {}, CreatePlayerInput['body']>,
  response: Response
) {
  try {
    const player = await createPlayer(request.body);
    return response.send(player);
  } catch (error: any) {
    console.log(error);
    return response.status(409).send(error.message);
  }
}

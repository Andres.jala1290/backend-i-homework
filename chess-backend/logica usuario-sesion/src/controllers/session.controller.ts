import { Request, Response } from 'express';
import config from 'config';
import {
  createSession,
  findSessions,
  updateSession
} from '../services/session.service';
import { validatePassword } from '../services/player.service';
import { signJwt } from '../utils/jwt.utils';

export async function createPlayerSessionHandler(
  request: Request,
  response: Response
) {
  const player = await validatePassword(request.body);

  if (!player) {
    return response.status(401).send('Invalid username or password');
  }

  const session = await createSession(
    player._id,
    request.get('player-agent') || ''
  );

  const accessToken = signJwt(
    {
      _id: player._id,
      username: player.username,
      password: player.password,
      createdAt: player.createdAt,
      updatedAt: player.updatedAt,
      __v: player.__v,
      session: session._id
    },
    { expiresIn: config.get('accessTokenTimeToLive') }
  );

  const refreshToken = signJwt(
    {
      _id: player._id,
      username: player.username,
      password: player.password,
      createdAt: player.createdAt,
      updatedAt: player.updatedAt,
      __v: player.__v,
      session: session._id
    },
    { expiresIn: config.get('refreshTokenTimeToLive') }
  );

  return response.send({
    accessToken,
    refreshToken
  });
}

export async function getPlayerSessionsHandler(
  request: Request,
  response: Response
) {
  const playerId = response.locals.player._id;

  const sessions = await findSessions({ player: playerId, valid: true });

  return response.send(sessions);
}

export async function deleteSessionHandler(
  request: Request,
  response: Response
) {
  const sessionId = response.locals.player.session;

  await updateSession({ _id: sessionId }, { valid: false });

  return response.send({
    accessToken: null,
    refreshToken: null
  });
}

import express from 'express';
import config from 'config';
import connect from './utils/connect';
import routes from './routes';
import deserializePlayer from './middleware/deserializePlayer';

const port = config.get<number>('port');

const app = express();

app.use(express.json());

app.use(deserializePlayer);

app.listen(port, async () => {
  console.log(`App is running at http://localhost:${port}`);

  await connect();

  routes(app);
});

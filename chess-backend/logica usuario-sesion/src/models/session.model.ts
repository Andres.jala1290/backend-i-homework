import mongoose from 'mongoose';
import { PlayerDocument } from './player.model';

export interface SessionDocument extends mongoose.Document {
  player: PlayerDocument['_id'];
  valid: boolean;
  playerAgent: string;
  createdAt: Date;
  updatedAt: Date;
}

const sessionSchema = new mongoose.Schema(
  {
    player: { type: mongoose.Schema.Types.ObjectId, ref: 'Player' },
    valid: { type: Boolean, default: true },
    playerAgent: { type: String }
  },
  {
    timestamps: true
  }
);

const SessionModel = mongoose.model<SessionDocument>('Session', sessionSchema);

export default SessionModel;

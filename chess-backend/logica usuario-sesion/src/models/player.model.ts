import mongoose from 'mongoose';
import bcrypt from 'bcrypt';
import config from 'config';

export interface PlayerDocument extends mongoose.Document {
  username: string;
  password: string;
  createdAt: Date;
  updatedAt: Date;

  comparePassword(candidatePassword: string): Promise<boolean>;
}

const playerSchema = new mongoose.Schema(
  {
    username: { type: String, required: true, unique: true },
    password: { type: String, required: true }
  },
  {
    timestamps: true
  }
);

playerSchema.pre('save', async function (next) {
  let player = this as PlayerDocument;

  if (!player.isModified('password')) {
    return next();
  }

  const salt = await bcrypt.genSalt(config.get<number>('saltWorkFactor'));

  const hash = await bcrypt.hashSync(player.password, salt);

  player.password = hash;

  return next();
});

playerSchema.methods.comparePassword = async function (
  candidatePassword: string
): Promise<boolean> {
  const player = this as PlayerDocument;

  return bcrypt
    .compare(candidatePassword, player.password)
    .catch((error) => false);
};

const PlayerModel = mongoose.model<PlayerDocument>('Player', playerSchema);

export default PlayerModel;

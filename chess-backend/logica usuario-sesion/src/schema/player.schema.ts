import { number, object, string, TypeOf } from 'zod';

export const createPlayerSchema = object({
  body: object({
    username: string({
      required_error: 'Username is required'
    }),
    password: string({
      required_error: 'Password is required'
    }).min(6, 'Password too short - should be 6 chars minimum'),
    passwordConfirmation: string({
      required_error: 'Password confirmation is required'
    })
  }).refine((data) => data.password === data.passwordConfirmation, {
    message: 'Passwords do not match',
    path: ['passwordConfirmation']
  })
});

export type CreatePlayerInput = Omit<
  TypeOf<typeof createPlayerSchema>,
  'body.passwordConfirmation'
>;

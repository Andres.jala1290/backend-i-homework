import { DocumentDefinition, FilterQuery } from 'mongoose';
import PlayerModel, { PlayerDocument } from '../models/player.model';

export async function createPlayer(
  input: DocumentDefinition<
    Omit<PlayerDocument, 'createdAt' | 'updatedAt' | 'comparePassword'>
  >
) {
  try {
    return await PlayerModel.create(input);
  } catch (error: any) {
    throw new Error(error);
  }
}

export async function validatePassword({
  username,
  password
}: {
  username: string;
  password: string;
}) {
  const player = await PlayerModel.findOne({ username });

  if (!player) {
    return false;
  }

  const isValid = await player.comparePassword(password);

  if (!isValid) {
    return false;
  }

  return player;
}

export async function findPlayer(query: FilterQuery<PlayerDocument>) {
  return PlayerModel.findOne(query).lean();
}

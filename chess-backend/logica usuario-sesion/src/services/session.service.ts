import config from 'config';
import { get } from 'lodash';
import { FilterQuery, UpdateQuery } from 'mongoose';
import SessionModel, { SessionDocument } from '../models/session.model';
import { signJwt, verifyJwt } from '../utils/jwt.utils';
import { findPlayer } from './player.service';

export async function createSession(playerId: string, playerAgent: string) {
  const session = await SessionModel.create({ player: playerId, playerAgent });

  return session.toJSON();
}

export async function findSessions(query: FilterQuery<SessionDocument>) {
  return SessionModel.find(query).lean();
}

export async function updateSession(
  query: FilterQuery<SessionDocument>,
  update: UpdateQuery<SessionDocument>
) {
  return SessionModel.updateOne(query, update);
}

export async function reIssueAccessToken({
  refreshToken
}: {
  refreshToken: string;
}) {
  const { decoded } = verifyJwt(refreshToken);
  if (!decoded || !get(decoded, 'session')) {
    return false;
  }

  const session = await SessionModel.findById(get(decoded, 'session'));

  if (!session || !session.valid) return false;

  const player = await findPlayer({ _id: session.player });

  if (!player) return false;

  const accessToken = signJwt(
    {
      _id: player._id,
      username: player.username,
      password: player.password,
      createdAt: player.createdAt,
      updatedAt: player.updatedAt,
      __v: player.__v,
      session: session._id
    },
    { expiresIn: config.get('accessTokenTimeToLive') }
  );

  return accessToken;
}

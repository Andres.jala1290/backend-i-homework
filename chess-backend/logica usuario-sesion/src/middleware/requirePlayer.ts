import { Request, Response, NextFunction } from 'express';

const requirePlayer = (
  request: Request,
  response: Response,
  nextFunction: NextFunction
) => {
  const player = response.locals.player;

  if (!player) {
    return response.sendStatus(403);
  }

  return nextFunction();
};

export default requirePlayer;

import { get } from 'lodash';
import { Request, Response, NextFunction } from 'express';
import { verifyJwt } from '../utils/jwt.utils';
import { reIssueAccessToken } from '../services/session.service';

const deserializePlayer = async (
  request: Request,
  response: Response,
  nextFunction: NextFunction
) => {
  const accessToken = get(request, 'headers.authorization', '').replace(
    /^Bearer\s/,
    ''
  );

  const refreshToken = get(request, 'headers.x-refresh');

  if (!accessToken) {
    return nextFunction();
  }

  const { decoded, expired } = verifyJwt(accessToken);

  if (decoded) {
    response.locals.player = decoded;
    return nextFunction();
  }

  if (expired && refreshToken) {
    const newAccessToken = await reIssueAccessToken({ refreshToken });

    if (newAccessToken) {
      response.setHeader('x-access-token', newAccessToken);
    }

    const result = verifyJwt(newAccessToken.toString());

    response.locals.player = result.decoded;
    return nextFunction();
  }

  return nextFunction();
};

export default deserializePlayer;

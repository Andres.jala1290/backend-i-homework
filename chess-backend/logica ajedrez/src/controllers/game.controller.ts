import { Request, Response } from 'express';
import { createGame } from '../services/game.service';

export async function manageGameHandler(request: Request, response: Response) {
  try {
    if (request.body.gameState === 'new') {
      const game = await createGame();
      return response.send(game);
    }
    return response.sendStatus(200);
  } catch (error: any) {
    console.log(error);
    return response.status(409).send(error.message);
  }
}

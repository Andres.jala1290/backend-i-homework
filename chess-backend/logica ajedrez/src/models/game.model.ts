import { Coordinate } from '../core/coordinate';
import { IN_GAME, READY_TO_PLAY } from '../core/gameStatus';
import Board from './board.model';

export default class Game {
  constructor(
    public status: number,
    private board: Board,
    private teamTurn: string
  ) {}

  start(): void {
    this.status = IN_GAME;
  }

  reset(): void {
    this.board = new Board();
    this.status = READY_TO_PLAY;
  }

  movePiece(
    movingPieceCoordinate: Coordinate,
    targetedSpace: Coordinate
  ): boolean {
    if (this.status === IN_GAME) {
      if (
        this.board.getPiece(movingPieceCoordinate)?.getTeam() === this.teamTurn
      ) {
        this.board.movePiece(movingPieceCoordinate, targetedSpace);
      }
    }
    return false;
  }
}

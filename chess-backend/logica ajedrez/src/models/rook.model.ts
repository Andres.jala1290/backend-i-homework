import { Coordinate } from '../core/coordinate';
import Piece from './piece.interface';

export default class Rook implements Piece {
  constructor(private team: string) {
    this.team = team;
  }

  getTeam() {
    return this.team;
  }

  isValidMove(position: Coordinate, target: Coordinate): boolean {
    throw new Error('Method not implemented.');
  }
}

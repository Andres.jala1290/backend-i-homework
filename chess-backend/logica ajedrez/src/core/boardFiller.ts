import Bishop from '../models/bishop.model';
import Horse from '../models/horse.model';
import King from '../models/king.model';
import Pawn from '../models/pawn.model';
import Piece from '../models/piece.interface';
import Queen from '../models/queen.model';
import Rook from '../models/rook.model';
import { Coordinate } from './coordinate';
import { BLACK_PIECES, WHITE_PIECES } from './teamColor';

export default function fillBoard() {
  const board = new Map<string, Piece>();

  fillPawns(board);
  fillRooks(board);
  fillHorses(board);
  fillBishops(board);
  fillQueens(board);
  fillKings(board);

  return board;
}

function fillPawns(board: Map<string, Piece>) {
  for (let column = 0; column < 8; column++) {
    const newCoordinate = new Coordinate(1, column);

    board.set(newCoordinate.toKey(), new Pawn(WHITE_PIECES));
  }

  for (let column = 0; column < 8; column++) {
    const newCoordinate = new Coordinate(6, column);

    board.set(newCoordinate.toKey(), new Pawn(BLACK_PIECES));
  }
}

function fillRooks(board: Map<string, Piece>) {
  let newCoordinate;

  newCoordinate = new Coordinate(0, 0);
  board.set(newCoordinate.toKey(), new Rook(WHITE_PIECES));
  newCoordinate = new Coordinate(0, 7);
  board.set(newCoordinate.toKey(), new Rook(WHITE_PIECES));

  newCoordinate = new Coordinate(7, 0);
  board.set(newCoordinate.toKey(), new Rook(BLACK_PIECES));
  newCoordinate = new Coordinate(7, 7);
  board.set(newCoordinate.toKey(), new Rook(BLACK_PIECES));
}

function fillHorses(board: Map<string, Piece>) {
  let newCoordinate;

  newCoordinate = new Coordinate(0, 1);
  board.set(newCoordinate.toKey(), new Horse(WHITE_PIECES));
  newCoordinate = new Coordinate(0, 6);
  board.set(newCoordinate.toKey(), new Horse(WHITE_PIECES));

  newCoordinate = new Coordinate(7, 1);
  board.set(newCoordinate.toKey(), new Horse(BLACK_PIECES));
  newCoordinate = new Coordinate(7, 6);
  board.set(newCoordinate.toKey(), new Horse(BLACK_PIECES));
}

function fillBishops(board: Map<string, Piece>) {
  let newCoordinate;

  newCoordinate = new Coordinate(0, 2);
  board.set(newCoordinate.toKey(), new Bishop(WHITE_PIECES));
  newCoordinate = new Coordinate(0, 5);
  board.set(newCoordinate.toKey(), new Bishop(WHITE_PIECES));

  newCoordinate = new Coordinate(7, 2);
  board.set(newCoordinate.toKey(), new Bishop(BLACK_PIECES));
  newCoordinate = new Coordinate(7, 5);
  board.set(newCoordinate.toKey(), new Bishop(BLACK_PIECES));
}

function fillQueens(board: Map<string, Piece>) {
  let newCoordinate;

  newCoordinate = new Coordinate(0, 3);
  board.set(newCoordinate.toKey(), new Queen(WHITE_PIECES));

  newCoordinate = new Coordinate(7, 3);
  board.set(newCoordinate.toKey(), new Queen(BLACK_PIECES));
}

function fillKings(board: Map<string, Piece>) {
  let newCoordinate;

  newCoordinate = new Coordinate(0, 4);
  board.set(newCoordinate.toKey(), new King(WHITE_PIECES));

  newCoordinate = new Coordinate(7, 4);
  board.set(newCoordinate.toKey(), new King(BLACK_PIECES));
}

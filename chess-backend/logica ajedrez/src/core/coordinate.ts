export class Coordinate {
  constructor(public row: number, public column: number) {}

  public toKey(): string {
    return this.row.toString() + this.column.toString();
  }
}
